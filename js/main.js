let moduleCarte = (function(){
    let genererTitre = function(){
        return HolderIpsum.words(Math.floor(Math.random() * 2) + 1, true);
    }
    
    let genererSousTitre = function(){
        return HolderIpsum.words(Math.floor(Math.random() * 5) + 2, true);
    }

    let genererDescription = function(){
        return HolderIpsum.words(Math.floor(Math.random() * 10) + 10, true);
    }

    let genererImageSrc = function(){
        return `https://avatars.dicebear.com/v2/jdenticon/${ Math.random() * 1000000 }.svg`;
    };
})();
